#!/usr/bin/env python
# coding: utf-8

# # LokSabha Election EDA
# #By- Aarush Kumar
# #Dated: August 16,2021

# In[3]:


import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
import seaborn as sns
plt.style.use('fivethirtyeight')


# In[14]:


data = pd.read_csv('/home/aarush100616/Downloads/Projects/LokSabha Election EDA/LS_2.0.csv')


# In[15]:


data


# In[16]:


#Rename Multiple columns in a dataframe.
data.rename(columns = {'TOTAL\nVOTES':'TOTALVOTES','CRIMINAL\nCASES':'CRIMINALCASES'}, inplace = True)


# In[17]:


data.isnull().sum()


# In[18]:


data.EDUCATION.unique()


# In[19]:


#Replace multiple words in a column for certain words.
def update_education(row):
    if row['EDUCATION'] in ['Illiterate','Not Available']:
        return 'Iliterate/NA'
    if row['EDUCATION'] in ['5th Pass','8th Pass','10th Pass','12th Pass']:
        return 'School Level'
    elif row['EDUCATION'] in ['Graduate Professional','Graduate']: 
        return 'Graduate'
    elif row['EDUCATION'] in ['Post Graduate\n','Post Graduate', 'Doctorate']:
        return 'Post Graduate/Doctorate'
    else:
        'Literate'
        
data['Education_New'] = data.apply(update_education,axis =1)


# In[20]:


data['CRIMINALCASES'] = data['CRIMINALCASES'].replace({'Not Available': 0})
data['CRIMINALCASES'] = data['CRIMINALCASES'].fillna(0)


# In[21]:


data.isnull().sum()


# In[22]:


data.Education_New.unique()


# In[23]:


#Replace NA in Education_New wth Iliterate/NA.
data['Education_New'].fillna('Iliterate/NA', inplace=True)


# In[24]:


help(sns.set)


# In[25]:


#Sort Countplot by using Order
#Filter applied on column 'Winner', for not equal to 0. And with that filter applied, count of column 'Education_New' is applied.
sns.countplot(x=data[data['WINNER'] != 0]['Education_New'],               
              order=data[data['WINNER'] != 0]['Education_New'].value_counts().index,hue = data['GENDER'], saturation=1,
                  edgecolor=(0,0,0),
                  linewidth=2)
plt.xticks(rotation=45)
plt.show()


# In[26]:


sns.countplot(x=data[data['WINNER'] != 0]['CATEGORY'],               
              order=data[data['WINNER'] != 0]['CATEGORY'].value_counts().index,hue = data['GENDER'], saturation=1,
                  edgecolor=(0,0,0),
                  linewidth=2)
plt.xticks(rotation=45)
plt.show()


# In[27]:


#Countplot of varies category, with a hue of winner.
sns.countplot(x=data['CATEGORY'],               
              order=data['CATEGORY'].value_counts().index,hue = data['WINNER'], saturation=1,
                  edgecolor=(0,0,0),
                  linewidth=2)
plt.xticks(rotation=45)
plt.show()


# In[28]:


#Filter for value not equal to 0 in column Winner, and then count of winner of each category.
new_df = data[data['WINNER'] != 0].CATEGORY.value_counts().reset_index() #Use .reset.index() to store it as a DF
new_df 


# In[29]:


#Rename of Columns
new_df.rename(columns = {'index':'Category','CATEGORY':'Count'}, inplace = True)
new_df 


# In[30]:


#Set Category as index so Pie Chart can use it as a label.
new_df.set_index('Category', inplace= True)


# In[31]:


new_df.plot.pie(y='Count', explode = (0.05,0,0.1),autopct='%1.1f%%')


# In[32]:


#Filter for value not equal to 0 in column Winner, and then count of win of each Party and save as a list.
new_df2 = data[data['WINNER'] != 0].PARTY.value_counts()
new_df2


# In[33]:


pie, ax = plt.subplots(figsize=[30,8]) #To Size the Pie Chart
new_df2.plot.pie(x=new_df2,autopct='%1.1f%%',startangle=90, shadow=True, legend = False)
plt.title("Wins per Party", fontsize=14)


# In[34]:


top_20parties = pd.Series(data['PARTY'].value_counts().head(21))
top_20parties


# In[35]:


top_20parties = top_20parties.index.drop(['IND'])
top_20parties


# In[36]:


#Parties groupby most seats won, Top 5.
Vote_share=data.groupby("PARTY")["WINNER"].sum().nlargest(5).index.tolist()
Vote_share


# In[37]:


def fuc(row):
    if row["PARTY"] not in Vote_share:
        return("Others")
    else:
        return row['PARTY']
data['Party_new']=data.apply(fuc,axis=1)
data.head()
Top5=data.groupby("Party_new")["TOTALVOTES"].sum()
explode = (0.1,0.1,0.1,0,0.1,0)
Top5_index=Top5.index
Top5_label=Top5.values
Top5.plot.pie(labels=Top5_index,
        shadow=True, startangle=0,explode = explode,autopct='%1.1f%%');

